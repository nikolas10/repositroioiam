import persistencia
import numpy as np
import datos
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error, confusion_matrix, precision_score, accuracy_score
import warnings
warnings.filterwarnings('ignore')
import os

modelos = list()
tipos = ['Regresion Logistica', 'Perceptron' , 'Perceptron Multicapa', 
		'Arbol de decision', 'Maquina Vector Soporte']
split_test = [0.1 ,0.2, 0.3, 0.4, 0.5]

#-------
def menuGraficar():
	op = 'S'

	while op.upper() == 'S':
		os.system('cls')
		print("------------Aprendizaje de Maquina------------\n")
		print(" 1-Graficar Exactitud (accuracy_score) - TABLA2")
		print(" 2-Graficar Precisión (precision_score) - TABLA2")
		print(" 3-Graficar Exactitud (accuracy_score) - TABLA3")
		print(" 4-Graficar Precisión (precision_score) - TABLA3")
		prp = int(input(" "))
		
		if prp == 1 or prp == 2:
			tabla = 'TABLA2'
		else:
			tabla = 'TABLA3'

		xx = np.array(split_test)
		x,y = datos.load_excel(tabla)
		for t in tipos:
			m = persistencia.cargarModelo(t,tabla[-1])
			yy = list()
			for sp in split_test:
				yy_ = list()
				#x_train,x_test,y_train,y_test = train_test_split(x,y,test_size = sp)
				x_train,x_test,y_train,y_test = train_test_split(x,y,test_size = sp, random_state = np.random.RandomState(42))
				y_prediccion = m.predict(x_test)
				#yy.append(1 - np.mean(y_prediccion == y_test))
				if prp == 1 or prp == 3:
					yy.append(accuracy_score(y_test,y_prediccion))
				else:
					yy.append(precision_score(y_test,y_prediccion))
			plt.plot(xx, yy, label=t+" "+tabla)

		plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left', ncol=2, mode="expand", borderaxespad=0.)
		plt.xlabel("Proporción de datos para prueba")
		if prp == 1 or prp == 3:
			plt.ylabel("EXACTITUD")
		else:
			plt.ylabel("PRECISION")
		plt.show()

		op = input('DESEA SEGUIR GRAFICANDO? S/N\t')
