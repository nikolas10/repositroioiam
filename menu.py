import datos
import os
class Menu():
	def __init__(self):
		self.opcionTabla = None
		self.opcionAlgoritmo = None
		self.algoritmos = ['Regresion Logistica', 'Perceptron' , 'Perceptron Multicapa', 
							'Arbol de decision', 'Maquina Vector Soporte']
		self.opcionPrincipal = None
		self.opcionPredictor = None
	def seleccionPrincipal(self):
		print(" 1-ENTRENAR")
		print(" 2-PREDECIR")
		print(" 3-GRAFICAR")
		print(" 4-BUSCAR MEJOR")
		self.opcionPrincipal = int(input(" "))
		self.limpiar()
		return self.opcionPrincipal

	def seleccionTabla(self):
		print(" 1-TRABAJAR CON TABLA 2")
		print(" 2-TRABAJAR CON TABLA 3")
		self.opcionTabla = 'TABLA'+str(int(input(" "))+1)
		self.limpiar()
		return self.opcionTabla

	def seleccionAlgoritmo(self):
		num = 1
		for o in self.algoritmos:
			print(f' {num}-{o}')
			num+=1
		self.opcionAlgoritmo = self.algoritmos[int(input(" "))-1]
		self.limpiar()
		return self.opcionAlgoritmo

	def seleccionPredictor(self):
		num = 1
		for o in self.algoritmos:
			print(f' {num}-Predecir con: {o}')
			num+=1
		print(f' {num}-USAR EL MEJOR')
		op = int(input(" "))
		if op == num:
			self.opcionPredictor = 'MEJOR'
		else:
			self.opcionPredictor = self.algoritmos[op-1]
		self.limpiar()
		return self.opcionPredictor

	def limpiar(self):
		os.system('cls')
		print("------------Aprendizaje de Maquina------------\n")

	def mostrarEstado(self):
		self.limpiar()
		print(f'DATOS: {self.opcionTabla}, ALGORITMO: {self.opcionAlgoritmo}')




