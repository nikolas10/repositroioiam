#from sklearn.preprocessing import OneHotEncoder

from sklearn.linear_model import LogisticRegression, Perceptron, LogisticRegressionCV, PassiveAggressiveClassifier,SGDClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import mean_squared_error, confusion_matrix, precision_score, accuracy_score, recall_score, f1_score
from sklearn.model_selection import cross_val_score, train_test_split, cross_val_predict
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
import os
import persistencia
import parametros
import numpy as np
import warnings
warnings.filterwarnings('ignore')

class ALGORITMO:
	def __init__(self,x,y):
		self.caracteristicas = x
		self.etiquetas = y
		self.split = None
		self.matriz = None
		self.exactitud = None
		self.precision = None
		self.parametros = None
		self.algoritmo = None
		self.entrenado = None
		self.score = None
		self.random_split = True
		self.indiceM = None
		
	def Mostrarme(self):
		print(f'{self.algoritmo}')
		for k,v in self.parametros.items():
			print(f"\t {k}: {v}")
		print(f'\t split: {self.split}')
		print(f'\t matriz: {self.matriz}')
		print(f'\t exactitud: {self.exactitud}')
		print(f'\t score: {self.score}')
		print(f'\t precision: {self.precision}')

	def Ejecutarme(self):
		if self.algoritmo == 'Regresion Logistica':
			aed = LogisticRegression(**self.parametros)
		elif self.algoritmo == 'Perceptron':
			aed = Perceptron(**self.parametros)
		elif self.algoritmo == 'Perceptron Multicapa':
			aed = MLPClassifier(**self.parametros)
		elif self.algoritmo == 'Arbol de decision':
			aed = DecisionTreeClassifier(**self.parametros)
		else:
			aed = SVC(**self.parametros)

		if self.random_split:
			x_train,x_test,y_train,y_test = train_test_split(self.caracteristicas,self.etiquetas,test_size = self.split)
			#x_train,x_test,y_train,y_test = train_test_split(self.caracteristicas,self.etiquetas,test_size = self.split, random_state = np.random.RandomState(42))
		else:
			x_train = self.caracteristicas[200:-1]
			x_test = self.caracteristicas[:200]
			y_train = self.etiquetas[200:-1]
			y_test = self.etiquetas[:200]
		
		#ENTRENAR
		aed.fit(x_train,y_train)

		#TESTEAR
		y_prediccion = aed.predict(x_test)
		
		#ANALISIS DE RENDiMiENTO
		self.entrenado = aed
		self.matriz = confusion_matrix(y_test,y_prediccion).tolist()
		self.exactitud = accuracy_score(y_test,y_prediccion)
		self.score = aed.score(self.caracteristicas,self.etiquetas)
		self.precision = precision_score(y_test,y_prediccion)
		#self.indiceMatriz()

	def Guardar(self,mejor = None):
		if len(self.caracteristicas.columns.tolist()) > 9:
			tipo = '2'
		else:
			tipo = '3'
		
		persistencia.guardarModelo(self.entrenado,self.algoritmo,tipo,mejor)

	def getOrden(self):
		if parametros.CRITERIO == 'score':
			return self.score
		elif parametros.CRITERIO == 'exactitud':
			return self.exactitud
		elif parametros.CRITERIO == 'precision':
			return self.precision
		else:
			return self.indiceM

	def indiceMatriz(self):
		total_positivos = sum(self.matriz[0])
		total_negativos = sum(self.matriz[1])
		total = total_positivos + total_negativos
		bien = self.matriz[0][0] + self.matriz[1][1]
		self.indiceM = bien * 100 / total