import modelos

#CRITERIO = 'score'
CRITERIO = 'exactitud'
#CRITERIO = 'precision'

def parametros_regresion_lineal():
	tol = [1e-4,1e-3,1e-2]
	solver = ["newton-cg", "lbfgs", "liblinear"]
	C = (1,0.1,0.01)
	penalty = ['l1', 'l2']
	fit_intercept = [True,False]
	class_weight = 'balanced'
	max_iter = 2000
	todos = list()
	for t in tol:
		for cc in C:
			for f in fit_intercept:
				for s in solver:
					for p in penalty:
						if s != 'liblinear' and p == 'l1':
							pass
						else:
							sal={'tol':t,'solver':s,'C':cc,'fit_intercept':f,'penalty':p,'class_weight':class_weight,
								'max_iter':max_iter}
							todos.append(sal)
	return todos


def parametros_perceptron():
	tol = [1e-4,1e-3,1e-2]
	penalty = ['l1', 'l2', 'elasticnet']
	alpha = [0.00001,0.0001, 0.001, 0.01]
	fit_intercept = [True,False]
	class_weight = 'balanced'
	max_iter = 2000
	todos = list()
	for t in tol:
		for p in penalty:
			for a in alpha:
				for f in fit_intercept:
					for p in penalty:
						sal={'tol':t,'penalty':p,'alpha':a,'fit_intercept':f,'class_weight':class_weight,
								'max_iter':max_iter}
						todos.append(sal)
	return todos

def parametros_perceptron_multicapa():
	tol = [1e-4,1e-3,1e-2]
	activation = ['identity', 'logistic', 'tanh', 'relu']
	solver = ['lbfgs', 'sgd', 'adam']
	learning_rate = ['constant', 'invscaling', 'adaptive']
	alpha = 0.0001 					#NO HACE FALTA PASARLO, YA ES ASI POR DEFAULT
	learning_rate_init = 0.001 		#NO HACE FALTA PASARLO, YA ES ASI POR DEFAULT	
	max_iter = 2000
	todos = list()

	for t in tol:
		for a in activation:
			for s in solver:
				for lr in learning_rate:
					if s != 'sdg' and lr != 'constant':
						pass
					else:
						sal={'tol':t,'activation':a,'solver':s,'learning_rate':lr,
								'max_iter':max_iter}
						todos.append(sal)
	return todos
	
def parametros_arbol():
	criterion = ['gini', 'entropy']
	splitter = ['best','random']
	max_features = [None,1.0,0.5,'auto', 'sqrt', 'log2']
	todos = list()
	for c in criterion:
		for s in splitter:
			for m in max_features:
				sal = {'criterion':c,'splitter':s,'max_features':m}
				todos.append(sal)

	return todos

def parametros_svm():
	gamma = [1.0, 'scale', 'auto']
	kernel = ['linear', 'rbf', 'sigmoid']
	tol = [1e-4,1e-3,1e-2]
	C = [1,0.1,0.01]
	class_weight = 'balanced'
	max_iter = -1
	todos = list()
	for k in kernel:
		for g in gamma:
			if k == 'linear':
				g = None
			for t in tol:
				for cc in C:
					if g:
						sal = {'gamma':g,'kernel':k,'tol':t,'C':cc,'class_weight':class_weight,'max_iter':max_iter}
					else:
						sal = {'kernel':k,'tol':t,'C':cc,'class_weight':class_weight,'max_iter':max_iter}
					todos.append(sal)
	return todos
