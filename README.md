# Aprendizaje de Maquina para predecir el rendimiento academico.

_Este proyecto busca predecir, a partir del rendimiento de un alumno en el Seminario de ingreso, si éste llega a cursar la materia de segundo año "Sintaxis y Semántica de los Lenguajes" de la carrera Ingenieria en Sistemas de Información de la Facultad Regional Resistencia de la Universidad Tecnológica Nacional._


### Pre-requisitos 📋

_El proyecto esta desarrollado en python 3.7.3 (se recomienda usar esta version), para consultar las librerias acceda a la carpeta requerimientos._
_para instalar las librerias ejecute el siguiente comando_
```
pip install -r requirements/requirements.txt
```

## Autores ✒️

_El proyecto fue realizado por estudiantes de la asignatura Introducción al Aprendizaje de Máquina del del Doctorado en Informática dictado en forma conjunta por la Facultad Regional Resistencia de la Universidad Tecnológica Nacional (UTN-FRRe), la Facultad de Ciencias Exactas y Naturales y Agrimensura de la Universidad Nacional del Nordeste (FaCENA-UNNE), y la Facultad de Ciencias Exactas, Químicas y Naturales de la Universidad Nacional de Misiones (FCEQyN-UNaM), bajo la supervisión de su profesor Dr. Eduardo Zamudio._

* **Gabriela Tomaselli**
* **Nicolas Tortosa**
