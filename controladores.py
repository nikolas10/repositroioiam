import modelos
import parametros
import persistencia

def Entrenamiento(caracteristicas,etiquetas, cual,save=True):
	if cual == 'Regresion Logistica':
		prm = parametros.parametros_regresion_lineal()
	elif cual == 'Perceptron':
		prm = parametros.parametros_perceptron()
	elif cual == 'Perceptron Multicapa':
		prm = parametros.parametros_perceptron_multicapa()
	elif cual == 'Arbol de decision':
		prm = parametros.parametros_arbol()
	elif cual == 'Maquina Vector Soporte':
		prm = parametros.parametros_svm()
	resultados = list()
	for spl in (0.1,0.2,0.3):
		for p in prm:
			obj = modelos.ALGORITMO(caracteristicas,etiquetas)
			obj.algoritmo = cual
			obj.split = spl
			obj.parametros = p
			obj.Ejecutarme()
			resultados.append(obj)

	resultados = sorted(resultados, key=lambda k: k.getOrden())
	mejor = resultados[-1]
	if save:
		mejor.Guardar()
	
	return mejor

def Predecir(x, cual):
	if len(x.columns.tolist()) > 6:
		tipo = '2'
	else:
		tipo = '3'
	entrenado = persistencia.cargarModelo(cual,tipo)
	legajo = x['legajo']
	x.drop('legajo',axis=1,inplace=True)
	y = entrenado.predict(x)
	persistencia.guardarPrediccion(legajo,y,cual,tipo)
	return y

