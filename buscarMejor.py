
tipos = ['Regresion Logistica', 'Perceptron' , 'Perceptron Multicapa', 
		'Arbol de decision', 'Maquina Vector Soporte']
tablas = ['TABLA2','TABLA3']

import menu
import datos
import controladores


mejores2 = list()
mejores3 = list()
def BuscaMejor():
	for ti in tipos:
		por_modelo = list()
		for cant in range(20):
			x,y = datos.load_excel(tablas[0])
			mejor = controladores.Entrenamiento(x,y,ti,False)
			por_modelo.append(mejor)
		mejor = sorted(por_modelo, key=lambda k: k.getOrden())
		mejores2.append(mejor[-1])

	for ti in tipos:
		por_modelo = list()
		for cant in range(20):
			x,y = datos.load_excel(tablas[1])
			mejor = controladores.Entrenamiento(x,y,ti,False)
			por_modelo.append(mejor)
		mejor = sorted(por_modelo, key=lambda k: k.getOrden())
		mejores3.append(mejor[-1])

	menu_principal.limpiar()
	for m in mejores2:
		m.Guardar()
		m.Mostrarme()
		
	input("SEGUIMOS??")

	for m in mejores3:
		m.Guardar()
		m.Mostrarme()

	maximo2 = sorted(mejores2, key=lambda k: k.getOrden())[-1]
	maximo3 = sorted(mejores3, key=lambda k: k.getOrden())[-1]
	maximo2.Guardar(mejor=True)
	maximo3.Guardar(mejor=True)
	print("--------MEJOR TABLA 2--------")
	maximo2.Mostrarme()
	print("--------MEJOR TABLA 3--------")
	maximo3.Mostrarme()
