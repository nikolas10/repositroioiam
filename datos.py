import pandas as pd 
import os

def oneHot(df):
	columnas = df.columns.tolist()
	cantidad_columnas =len(columnas)
	etiqueta = df[columnas[-1]]
	df.drop(columnas[-1],axis=1,inplace=True)
	legajo = df[columnas[0]]
	df.drop(columnas[0],axis=1,inplace=True)
	ingreso = df[columnas[1]]
	df.drop(columnas[1],axis=1,inplace=True)
	sexo = df[columnas[2]]
	df.drop(columnas[2],axis=1,inplace=True)
	new_df = pd.DataFrame()
	for c in columnas[2:-1]:
		new_df = pd.concat([new_df,pd.get_dummies(df[c],prefix = c)],axis=1)
		df.drop([c],axis=1,inplace = True)

	new_df = pd.concat([new_df,etiqueta],axis=1)
	#new_df = pd.concat([legajo,new_df],axis=1)
	new_df = pd.concat([sexo,new_df],axis=1)
	new_df = pd.concat([ingreso,new_df],axis=1)
	return new_df

def load_excel(file,oneH=None):
	path = os.path.dirname(os.path.realpath(__file__)) + '\\datos\\'+file+'.xls'
	archivo = pd.ExcelFile(path)
	df = pd.read_excel(archivo, "datos")
	if oneH:
		df = oneHot(df)
	else:
		df.drop('legajo',axis=1,inplace=True)
	#CARACTERISTICAS
	X = df.iloc[:,:-1]
	#ETIQUETAS
	Y = df.SSL
	return (X,Y)

def load_nuevos(file):
	path = os.path.dirname(os.path.realpath(__file__)) + '\\datos\\'+file+'.xls'
	archivo = pd.ExcelFile(path)
	df = pd.read_excel(archivo, "datos")
	return df
