from joblib import dump, load
import pandas as pd 
from datetime import datetime
def guardarModelo(modelo,nombre,tipo,mejor):
	if mejor:
		dump(modelo, 'entrenados/MEJOR'+tipo+'.joblib')
	else:
		dump(modelo, 'entrenados/'+nombre+tipo+'.joblib')


def cargarModelo(nombre,tipo):
	return load('entrenados/'+nombre+tipo+'.joblib')


def guardarPrediccion(x,y,cual,tipo):
	salida = pd.concat([x,pd.DataFrame(y,columns=["CURSA-SSL"])],axis=1)
	dia = datetime.now().strftime('%Y%m%d-%H-%M-%S')
	arch = pd.ExcelWriter('predicciones/'+dia+'('+cual+')(tabla-'+tipo+').xlsx', engine='xlsxwriter')
	#libro = arch.book
	salida.to_excel(arch, sheet_name='prediccion',index = False)
	hoja = arch.sheets['prediccion']
	formato_escala = {'type': '2_color_scale', 'min_color': '#FF0000', 'max_color': '#00FF00'}
	hoja.conditional_format('B2:B265', formato_escala)

	arch.save()