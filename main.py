import menu
import datos
import controladores
import persistencia
import graficar
import buscarMejor
menu_principal = menu.Menu()
op = 'S'

while op.upper() == 'S':
	menu_principal.limpiar()
	prp = menu_principal.seleccionPrincipal()
	if prp == 2:
		menu_principal.seleccionTabla()
		menu_principal.seleccionPredictor()
		entrada = datos.load_nuevos('PREDECIR'+ menu_principal.opcionTabla[-1])
		salida = controladores.Predecir(entrada,menu_principal.opcionPredictor)
		print(f"PREDICCION GUARDADA, CONSULTE EN LA CARPETA PREDICCIONES BAJO EL NOMBRE DE: {menu_principal.opcionPredictor}.xlsx")
	elif prp == 3:
		graficar.menuGraficar()
	elif prp == 4:
		buscarMejor.BuscaMejor()
	else:
		menu_principal.seleccionTabla()
		menu_principal.seleccionAlgoritmo()
		x,y = datos.load_excel(menu_principal.opcionTabla)
		mejor = controladores.Entrenamiento(x,y,menu_principal.opcionAlgoritmo)
		menu_principal.mostrarEstado()
		mejor.Mostrarme()
	op = input('DESEA SEGUIR PROBANDO? S/N\t')